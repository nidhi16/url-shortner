function validUrl(str) {
    var pattern = new RegExp(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi);
    if(!pattern.test(str)) {
      return false;
    } else {
    return true;
   }
}

$(document).ready(function() {
    $('form').submit(function(event) {
        event.preventDefault();
        // $('div#prog').css('visibility','visible');
        var formData;
        formData = {
            'url': $('input[name=url]').val()
        };
        if(validUrl(formData.url)) {
            $('div#prog').css('visibility','visible');
            $.ajax({
                type: 'POST',
                url: 'submit/',
                data: formData
            }).fail(function() {
                console.log("Request Failed");
            }).done(function(data) {
                $('div#prog').css('visibility','hidden');
                console.log("Always Executed");
                console.log(data);
                document.getElementById('demo').innerHTML = "The new url is: " + data + '<br>' +'<a href="' + data + '" target="_blank">' + "[open in new window]" + '</a>';
            });
        }
        else {
           alert("Please enter a valid URL.");
        }
    });
});
