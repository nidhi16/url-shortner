from __future__ import unicode_literals

from django.db import models


# Create your models here.

class Urls(models.Model):
    original_url = models.CharField(max_length=3000)

    def __str__(self):
        return self.original_url
