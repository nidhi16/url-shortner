from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import Urls
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.sites.shortcuts import get_current_site
# Create your views here.


def index(request):
    return render(request, 'short/index.html')


@csrf_exempt
def submit(request):
    long_url = request.POST.get('url')
    q = Urls(original_url=long_url)
    q.save()
    url_id = q.id
    current_domain = get_current_site(request)
    current_url = "http://" + str(current_domain)
    current_url = current_url + reverse('short:redirect', args=(url_id,))
    return HttpResponse(current_url)


def redirect(request, url_id):
    print 'redirect of short'
    url = Urls.objects.get(id=url_id)
    return HttpResponseRedirect(url.original_url)


# gunicorn --env DJANGO_SETTINGS_MODULE=urlShort.settings urlShort.wsgi:application