from django.conf.urls import url
from . import views

app_name = 'short'

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^submit/', views.submit, name="submit"),
    url(r'^(?P<url_id>[0-9]+)/$', views.redirect, name="redirect"),
]
